const express = require('express');
const mysql = require('mysql');

function connect() {
    const connection = mysql.createConnection({
        host: '172.0.2.139',
        user: 'root',
        password: 'manager',
        database: 'dac',
        port:3307
    });

    connection.connect();

    return connection;
}

const app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/Customers', (request, response) => {
    const connection = connect();
    const statement = `select * from Customers`;
    connection.query(statement, (error, customers) => {
        console.log(error);
        console.log(customers);
        response.send(customers);
    })
}) 

app.listen(3000,'0.0.0.0', () => {
    console.log(`Server started on 3000`);
});

